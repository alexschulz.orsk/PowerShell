﻿#Test-WsManTest-WsMan
Import-Module ActiveDirectory
#[xml]$xmlConfig = Get-Content -Path ("c:\install\Powerbot.xml")
#$token = $xmlConfig.config.system.token
#$timeout = $xmlConfig.config.system.timeout.'#text'
#$users = (($xmlConfig.config.system.friends).Split(",")).Trim()

# Telegram URLs
#$URL_get = "https://api.telegram.org/bot$token/getUpdates"
#$URL_set = "https://api.telegram.org/bot$token/sendMessage"
#$URL_get_file = "https://api.telegram.org/file/bot$token/"
#$URL_sendLacation = "https://api.telegram.org/bot$token/sendLacation"

#$csv_path = "C:\install\КлиентОИКДиспетчер.csv"
#$c = Get-Credential
$N=3 #число циклов опроса
$name="*56-*"
#$name="s56-*hypv*"
#$name="s56-*"
$now=Get-Date

############################################## Start functions
function checkinfo ($HostName) {
$1pc= Get-ADComputer $HostName -Properties Description
if (($1pc.Description -match "недоступен") -or ($1pc.Description -match "Производитель: Модель: Серийный номер: Кол-во ЦП: Кол-во ЛогП: Жёсткий диск: 0,00Гб ОЗУ:0,00Гб MAC адрес:") -or ($1pc.Description -eq $Null))  {
if ( Test-Connection $HostName -Quiet ) {
$computerSystem = get-wmiobject Win32_ComputerSystem -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
write-host "Системная информация для ПК:" $computerSystem.Name -ForegroundColor Green
$computerBIOS = get-wmiobject Win32_BIOS -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
$computerOS = get-wmiobject Win32_OperatingSystem -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
$computerOSB = get-wmiobject Win32_OperatingSystem -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
$computerBLD = get-wmiobject Win32_OperatingSystem -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
$computerCPU = get-wmiobject Win32_Processor -ComputerName $HostName -ErrorAction SilentlyContinue  #-Credential $c
$computerHDD = Get-WmiObject Win32_LogicalDisk -ComputerName $HostName -Filter "DeviceID='C:'" -ErrorAction SilentlyContinue  #-Credential $c
$Man = "Производитель: "+ $computerSystem.Manufacturer
$Mod = "Модель: "+ $computerSystem.Model -replace "32373A0", "M92p" -replace "3237EF9", "M92p" -replace "3167BC8", "M71e" -replace "10A4S00R0D", "M93" -replace "3664AK9", "M72e" -replace "3267B69", "M72e Tiny" -replace "0833AL2", "M70e"
$Ser = "Серийный номер: "+ $computerBIOS.SerialNumber
$CPUs = "Кол-во ЦП: "+ $ComputerSystem.NumberOfProcessors
$Cores ="Кол-во ЛогП: "+ $ComputerSystem.NumberOfLogicalProcessors
$HDD = "Жёсткий диск: " + "{0:N2}" -f ($computerHDD.Size/1GB) + "Гб"
$RAM =  "ОЗУ:" + "{0:N2}" -f ($computerSystem.TotalPhysicalMemory/1GB) + "Гб"
#$OS = "ОС: "+$computerOS.caption -replace "Microsoft Windows 7 Enterprise , Service Pack: 1", "Win 7"
$OSB = "дата установки:" + $computerOSB.ConvertToDateTime(($computerOSB).InstallDate)+"дата загрузки:" +$computerOSB.ConvertToDateTime(($computerOSB).LastBootUpTime)
$Usr = $computerSystem.UserName
$Mac = "MAC адрес: " + (Get-WmiObject win32_networkadapterconfiguration -ComputerName $HostName  -Filter "IpEnabled = TRUE" | select macaddress).macaddress #-Credential $c
#$Check=$Man, $Mod, $Ser, $CPUs, $Cores, $HDD, $RAM, $OS, $OSB, $Urs, $Mac
$Check=$Man+" "+ $Mod+" "+ $Ser+" "+ $CPUs+" "+ $Cores+" "+ $HDD+" "+ $RAM+" "+ $OSB+" "+$Mac+" "+ $Urs}
else{write-host "ПК:" $HostName "недоступен" -ForegroundColor Red
$Check="ПК: "+$HostName+" недоступен, время последнего опроса "+$now
}
$1pc.Description=$Check
Set-ADComputer -Instance $1pc
}
else {
$Check=$1pc.Description
}
return $Check
}
############################################## end functions


$pc=Get-ADComputer -Filter 'Name -like $name'-Properties IPv4Address, Description, OperatingSystem, OperatingSystemVersion, LastLogonDate | where Enabled | select name, IPv4Address, Description, OperatingSystem, OperatingSystemVersion, LastLogonDate | sort name
$pc |%   { $_.Description=checkinfo($_."Name")} #один поток

#$ThrottleLimit=[math]::Truncate($pc.Count/$N)
#$s = New-PSSession -ComputerName $pc.name -ThrottleLimit $ThrottleLimit  #-ThrottleLimit 16
#$ScriptBlock ={$_.Description=checkinfo($_."Name")}
#Invoke-Command -Session $s -ScriptBlock $ScriptBlock
<#
workflow PCinfo
{
param($computerName)
InlineScript
{
$computers=$pc
}
foreach -parallel($computer in $computers) {$computer=checkinfo($computer) }
}
#>


<#
write-host "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄" -ForegroundColor Blue
$_."Name",$_."IPv4Address"
checkinfo($_."Name")
write-host "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀" -ForegroundColor Blue
#>

$pc | ConvertTo-Html > \\s56-wsus\C$\inetpub\wwwroot\ПК.html -Title "Характеристики ПК: $name -replace '*',''"
\\s56-wsus\C$\inetpub\wwwroot\ПК.html 
#$pc | ConvertTo-Html > C:\install\ПК.html  -Title "Характеристики ПК: $name" #-replace '*',''"
#C:\install\ПК.html 
$pc | ConvertTo-Csv > \\ves.orene.vlmrk.corp\dfs\invent\ПК.txt -Delimiter ";"