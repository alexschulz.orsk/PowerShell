﻿
$inst_path=""
$DPath="\\ves.orene.vlmrk.corp\dfs\deploy"
$HKCU_Uninstall="HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"

Function Get-UsersPO {
 Process {
 $Searcher = New-Object DirectoryServices.DirectorySearcher 
 $Searcher.Filter = ('(&(objectCategory=person)(anr='+$env:USERNAME+'))').ToString()
 $Searcher.SearchRoot = 'LDAP://OU=ПО ВЭС,OU=Административная структура,DC=ves,DC=orene,DC=vlmrk,DC=corp'
 $PathLDAP=$Searcher.Findone().path
 $Object = [adsi]$PathLDAP
 $Object.memberOf
 }      
}


Function Get-AllPOGroups{
$Searcher = New-Object DirectoryServices.DirectorySearcher 
$Searcher.Filter = '(&(objectCategory=Group)(CN=Пользователи-*))'
$Searcher.SearchRoot = 'LDAP://OU=Развертываемое,OU=Программное обеспечение,OU=Группы безопасности локальные,DC=ves,DC=orene,DC=vlmrk,DC=corp'
$Searcher.FindAll().path 
}


if ($env:PROCESSOR_ARCHITECTURE="AMD64") {
    $D_lnk_path="\x64\"
	$Vpath=$env:LOCALAPPDATA+"\VirtualStore\Program Files (x86)\"
	} else {
    $D_lnk_path="\x86\" 
	$Vpath=$env:LOCALAPPDATA+"\VirtualStore\Program Files\"
}


$userGroups=(Get-UsersPO) -like 'CN=Пользователи-*' -replace "CN=Пользователи-" -replace ",.+$"
$allGroups=(Get-AllPOGroups) -replace "LDAP://CN=Пользователи-" -replace ",.+$"

#$userGroups=(dsget user (dsquery user domainroot -samid $env:USERNAME) -memberof) -replace '"CN=Пользователи-' -replace ",.+$"
#$userGroups=((Get-ADUser $env:USERNAME -Properties memberof).memberof -like "CN=Пользователи-*" -replace "CN=Пользователи-" -replace ",.+$")
#$allGroups=(dsquery group domainroot -name Пользователи-*) -replace '"CN=Пользователи-' -replace ",.+$"
#$allGroups=((Get-ADGroup -filter {name -like "Пользователи-*" }).DistinguishedName -replace "CN=Пользователи-" -replace ",.+$")
$not_userGroups=$allGroups  | ? {$userGroups -notcontains $_}  
#Установка ПО
"Будет выполнена проверка установки ПО: "
$userGroups
if (!(Test-Path Registry::'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'))
{New-Item -Path Registry::'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'
"Создан HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" }

$userGroups  | % { 
 if (Test-Path ($DPath+"\"+$_)) 
 {if (Get-Item -Path Registry::'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\'$_'*') 
 {$_+" уже установлено"} else 
 {
 "Начало установки "+$_
    if (Test-Path ($DPath+"\"+$_+"\Ярлык"+$D_lnk_path)) 
    {$inst_path=$Vpath
    #$inst_path=$Vpath+"\"+$_
    $inst_lnk_path=$DPath+"\"+$_+"\Ярлык"+$D_lnk_path+"*.lnk"
    }else
    {$inst_path=$env:APPDATA+"\"
    $inst_lnk_path=$DPath+"\"+$_+"\Ярлык\*.lnk"
    }
 "установка в "+$inst_path
 "ярлык из "+ $inst_lnk_path
    $DD=$DPath+"\"+$_+"\Дистрибутив\*"
    $DD
    $x =Copy-Item $DD -Destination $inst_path -Recurse -ErrorAction silentlyContinue
    if (!$x){"Копирование дистрибутива завершено;"}#ПО уже установлено;
    else{
    $x
    if (Test-Path ($DPath+"\"+$_+"\Реестр")) { REGEDIT /s($DPath+"\"+$_+"\Реестр\*.reg") -Recurse  
     "Импорт реестра завершен;"
     }    
    }
    <#if ($_="FileZilla")
    {"Настройка FileZilla на FTP сервер"
     $SiteManagerPath = "$env:APPDATA\FileZilla\sitemanager.xml"
     If (!(Test-Path $SiteManagerPath)) 
     {
	 New-Item -Path "$env:APPDATA" -Type Directory -Name "FileZilla"
	 Copy-Item "\\ves.orene.vlmrk.corp\dfs\deploy\FileZilla\Конфигурация\sitemanager.xml" "$env:APPDATA\FileZilla\sitemanager.xml"
	 Start-Sleep -Seconds 15	
	 [XML]$Site = Get-Content -Path $SiteManagerPath
	 $Site.FileZilla3.Servers.Server.User = "$env:userdomain"+"\"+"$env:username"
	 $Site.Save($SiteManagerPath)
     }
    }
     #>
    Copy-Item $inst_lnk_path -Destination $env:USERPROFILE\Desktop\ -Recurse
    $registryPath='HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\'+$_
    New-Item -Path $registryPath -Verbose
    $inst_path=$inst_path+$_
    New-ItemProperty -Path $registryPath -Name "DisplayName" -Value $_
    New-ItemProperty -Path $registryPath -Name "Inno Setup: Setup Version" -Value "5.5.5 (a)"
    New-ItemProperty -Path $registryPath -Name "Inno Setup: App Path" -Value $inst_path
    New-ItemProperty -Path $registryPath -Name "InstallLocation" -Value $inst_path
    New-ItemProperty -Path $registryPath -Name "Inno Setup: Icon Group" -Value $_
    New-ItemProperty -Path $registryPath -Name "Inno Setup: User" -Value $env:USERNAME
    New-ItemProperty -Path $registryPath -Name "Inno Setup: Selected Tasks" -Value "desktopicon"
    New-ItemProperty -Path $registryPath -Name "Inno Setup: Deselected Tasks" -Value ""
    New-ItemProperty -Path $registryPath -Name "Inno Setup: Language" -Value "russian"
    New-ItemProperty -Path $registryPath -Name "Publisher" -Value "ПО ""Информэнергосвязь"" филиала ПАО ""МРСК Волги""-""Оренбургэнерго"""
    New-ItemProperty -Path $registryPath -Name "InstallDate" -Value Get-Date -format yyyyMMdd
    New-ItemProperty -Path $registryPath -Name "QuietUninstallString" -Value $inst_path"\unins000.exe /SILENT"
    New-ItemProperty -Path $registryPath -Name "UninstallString" -Value $inst_path"\unins000.exe"
    New-ItemProperty -Path $registryPath -Name "MajorVersion" -Value 00000001 -PropertyType DWORD -Force
    New-ItemProperty -Path $registryPath -Name "MinorVersion" -Value 00000001 -PropertyType DWORD -Force
    New-ItemProperty -Path $registryPath -Name "NoModify" -Value 00000001 -PropertyType DWORD -Force
    New-ItemProperty -Path $registryPath -Name "NoRepair" -Value 00000001 -PropertyType DWORD -Force
    #New-Item -Path Registry::'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\'$_
    "установка "+$_+" окончена;"
    <#if ($_="FileZilla")
    {"Настройка FileZilla на FTP сервер"
     $SiteManagerPath = "$env:APPDATA\FileZilla\sitemanager.xml"
     If (!(Test-Path $SiteManagerPath)) 
     {
	 New-Item -Path "$env:APPDATA" -Type Directory -Name "FileZilla"

	 Copy-Item "$DPath\FileZilla\Конфигурация\sitemanager.xml" "$env:APPDATA\FileZilla\sitemanager.xml"
	 Start-Sleep -Seconds 15	
	 [XML]$Site = Get-Content -Path $SiteManagerPath
	 $Site.FileZilla3.Servers.Server.User = "$env:userdomain"+"\"+"$env:username"
	 $Site.Save($SiteManagerPath)
     }
    }#>
 }
 } else {"Нет дистрибутива "+$_}
 "#############################################################################"
}
#Удаление ПО
"Будет выполнена проверка удаления ПО: "
$not_userGroups
"***********************************************"
$_=''
$not_userGroups | % { 
 if (Test-Path ($DPath+"\"+$_)) 
 {  
  
    if (Test-Path ($DPath+"\"+$_+"\Ярлык"+$D_lnk_path)) 
     {$inst_path=$Vpath
     $inst_lnk_path=$DPath+"\"+$_+"\Ярлык"+$D_lnk_path+"*.lnk"
     }else
     {$inst_path=$env:APPDATA +"\"
     $inst_lnk_path=$DPath+"\"+$_+"\Ярлык\*.lnk"
     }
    if (Test-Path ($DPath+"\"+$_+"\Дистрибутив")){
    cd($inst_path)
    Remove-Item(dir($DPath+"\"+$_+"\Дистрибутив\")-Name) -Recurse -ErrorAction silentlyContinue
    }
    if (Test-Path ($DPath+"\"+$_+"\Ярлык\*.lnk"))
     {cd($env:USERPROFILE+"\Desktop\")
     Remove-Item(dir($DPath+"\"+$_+"\Ярлык\*.lnk")-Name) -Recurse -ErrorAction silentlyContinue
     }
    if (Test-Path ($DPath+"\"+$_+"\Ярлык"+$D_lnk_path+"*.lnk"))
     {cd($env:USERPROFILE+"\Desktop\")
     Remove-Item(dir($DPath+"\"+$_+"\Ярлык"+$D_lnk_path+"*.lnk")-Name) -Recurse -ErrorAction silentlyContinue
     }
     #Remove-Item -Path Registry::$HKCU_Uninstall'\'$_'*'
     $_+" удалено;"
 }
}