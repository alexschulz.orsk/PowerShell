﻿
$sleep = "3"
$separator = " ",","
$option = [System.StringSplitOptions]::RemoveEmptyEntries
$csv = Import-Csv -Path "\\ves.orene.vlmrk.corp\DFS\deploy\LAN_print\Принтеры.csv" -Delimiter ";"
#$OS = Get-WmiObject -Class Win32_OperatingSystem

$csv | %{
 $name=$_."Имя принтера"
 $address=$_."IP адрес"
 $pc_names=$_."Список имён ПК через пробел".Split($separator, $option)
  Foreach ($pc in $pc_names)
  { 
   if ($env:ComputerName -match $pc) 
   {   
    if (!(Test-Path Registry::"HKLM\SYSTEM\CurrentControlSet\Control\Print\Printers\$name"))
    {
    if((Test-Connection $address -Quiet)) 
    {start-sleep $sleep
     if (Test-Path "C:\Program Files (x86)") {$driverpath="\\ves.orene.vlmrk.corp\DFS\deploy\LAN_print\Drivers\upd-pcl6-x64\install.exe"} else {$driverpath="\\ves.orene.vlmrk.corp\DFS\deploy\LAN_print\Drivers\upd-pcl6-x32\install.exe"}
    $p = Start-Process $driverpath -ArgumentList "/q /n$name /sm$address" -wait -NoNewWindow -PassThru
    
    if ($p.HasExited) {"Принтер <$name> успешно установлен"}
    start-sleep $sleep  
    }
    else {"Принтер <$name> не доступен по адресу $address"}
    }
    else {"Принтер <$name> уже установлен"}
   
  }
}
}
$Printer = Get-Printer -Name "*(копия*" 
Remove-Printer -InputObject $Printer
get-ChildItem -Path Registry::"HKLM\SYSTEM\CurrentControlSet\Control\Print\Printers\*(копия*" | Remove-Item -Recurse